how to run
======


```
1) git clone https://brevis@bitbucket.org/brevis/krnews.git
2) cd krnews
3) composer install
4) ./bin/console doctrine:migrations:migrate
5) ./bin/console news:collect --daemon (or create cron job ./bin/console news:collect)
6) ./bin/console server:run
7) open http://127.0.0.1:8000 
basic auth: 
  login: demo
  password: demo
```

Live demo: http://109.234.34.3
