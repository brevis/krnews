<?php

namespace Application\Migrations;

use Doctrine\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180522182508 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE hashtag (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_5AB52A615E237E06 (name), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE media (id INT AUTO_INCREMENT NOT NULL, news_item_id INT NOT NULL, type INT NOT NULL, uri VARCHAR(255) NOT NULL, INDEX IDX_6A2CA10C458B4EB8 (news_item_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE newsitem (id INT AUTO_INCREMENT NOT NULL, content VARCHAR(255) NOT NULL, published_at DATETIME NOT NULL, source_id BIGINT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE newsitem_hashtag (newsitem_id INT NOT NULL, hashtag_id INT NOT NULL, INDEX IDX_2660088467F576A9 (newsitem_id), INDEX IDX_26600884FB34EF56 (hashtag_id), PRIMARY KEY(newsitem_id, hashtag_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE media ADD CONSTRAINT FK_6A2CA10C458B4EB8 FOREIGN KEY (news_item_id) REFERENCES newsitem (id)');
        $this->addSql('ALTER TABLE newsitem_hashtag ADD CONSTRAINT FK_2660088467F576A9 FOREIGN KEY (newsitem_id) REFERENCES newsitem (id)');
        $this->addSql('ALTER TABLE newsitem_hashtag ADD CONSTRAINT FK_26600884FB34EF56 FOREIGN KEY (hashtag_id) REFERENCES hashtag (id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE newsitem_hashtag DROP FOREIGN KEY FK_26600884FB34EF56');
        $this->addSql('ALTER TABLE media DROP FOREIGN KEY FK_6A2CA10C458B4EB8');
        $this->addSql('ALTER TABLE newsitem_hashtag DROP FOREIGN KEY FK_2660088467F576A9');
        $this->addSql('DROP TABLE hashtag');
        $this->addSql('DROP TABLE media');
        $this->addSql('DROP TABLE newsitem');
        $this->addSql('DROP TABLE newsitem_hashtag');
    }
}
