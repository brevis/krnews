<?php

namespace AppBundle\Service\NewsProvider;

use Abraham\TwitterOAuth\TwitterOAuth;
use AppBundle\Entity\HashTag;
use AppBundle\Entity\Media;
use AppBundle\Entity\NewsItem;

class TwitterProvider implements NewsProviderInterface
{
    /** @var TwitterOAuth */
    protected $twitterOauth;

    /** @var string */
    protected $twitterUsername;

    /** @var int */
    protected $twitsCount;

    public function setTwitterOauth(TwitterOAuth $to)
    {
        $this->twitterOauth = $to;
    }

    public function setTwitterUsername($twitterUsername)
    {
        $this->twitterUsername = $twitterUsername;
    }

    public function setTwitsCount($twitsCount)
    {
        $this->twitsCount = $twitsCount;
    }

    /**
     * Get tweets from Twitter API
     *
     * @param $fromId
     * @return array
     * @throws \Exception
     */
    public function getNews($fromId)
    {
        $content = $this->twitterOauth->get("account/verify_credentials");

        if (isset($content->errors) && is_array($content->errors) && count($content->errors) > 0) {
            throw new \Exception($content->errors[0]->message);
        }

        $options = [
            'screen_name' => $this->twitterUsername,
            'count' => $this->twitsCount,
            'include_rts' => false,
            'exclude_replies' => true
        ];
        if ($fromId) {
            $options['since_id'] = $fromId;
        }
        $rawNewsData = $this->twitterOauth->get("statuses/user_timeline", $options);
        if (!is_array($rawNewsData) || count($rawNewsData) < 1) {
            return [];
        }

        $news = [];
        foreach ($rawNewsData as $rn) {
            $news[] = $this->buildNewsItem($rn);
        }
        return $news;
    }

    /**
     * Build NewsItem from raw data
     *
     * @param $rawData
     * @return NewsItem
     */
    protected function buildNewsItem($rawData)
    {
        $newsItem = new NewsItem();

        $newsItem->setContent($rawData->text);

        $newsItem->setSourceId((int)$rawData->id);

        $publishedAt = new \DateTime();
        $publishedAt->setTimestamp(strtotime($rawData->created_at));
        $newsItem->setPublishedAt($publishedAt);

        if (isset($rawData->entities) && isset($rawData->entities->hashtags) && count($rawData->entities->hashtags) > 0) {
            foreach ($rawData->entities->hashtags as $rawHt) {
                $ht = new HashTag();
                $ht->setName($rawHt->text);
                $newsItem->addHashtag($ht);
            }
        }

        if (isset($rawData->entities) && isset($rawData->entities->media) && count($rawData->entities->media) > 0) {
            foreach ($rawData->entities->media as $rawMedia) {
                $media = new Media();
                $media->setUri($rawMedia->media_url_https);
                $media->setType($rawMedia->type === 'photo' ? Media::TYPE_IMAGE : Media::TYPE_VIDEO);
                $newsItem->addMedia($media);
                $media->setNewsItem($newsItem);
            }
        }

        return $newsItem;
    }
}