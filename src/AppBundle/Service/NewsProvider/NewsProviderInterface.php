<?php

namespace AppBundle\Service\NewsProvider;

interface NewsProviderInterface
{
    public function getNews($fromId);
}