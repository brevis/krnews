<?php

namespace AppBundle\Service;

use AppBundle\Entity\HashTag;
use AppBundle\Entity\NewsItem;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Translation\TranslatorInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

use AppBundle\Service\NewsProvider\NewsProviderInterface;

class NewsService
{

    /** @var LoggerInterface */
    protected $logger;

    /** @var EntityManagerInterface */
    protected $em;

    /** @var TranslatorInterface */
    protected $translator;

    /** @var NewsProviderInterface */
    protected $newsProvider;

    /** @var \Knp\Component\Pager\Pagination\AbstractPagination */
    protected $paginator;

    /** Dependencies setters */

    public function setLogger(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    public function setEntityManager(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function setTranslator(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    public function setNewsProvider(NewsProviderInterface $provider)
    {
        $this->newsProvider = $provider;
    }

    public function setPaginator($paginator)
    {
        $this->paginator = $paginator;
    }

    public function downloadNews()
    {
        $addedCount = 0;

        $newsRepo = $this->em->getRepository('AppBundle:NewsItem');
        $hashTagsRepo = $this->em->getRepository('AppBundle:HashTag');

        $fromId = $newsRepo->getMaxSourceId();

        $news = $this->newsProvider->getNews($fromId);
        if (is_array($news) && count($news) > 0) {
            /** @var NewsItem $n */
            foreach ($news as $n) {
                if ($this->isSourceIdExists($n->getSourceId(), $newsRepo)) {
                    continue;
                }
                /** @var HashTag $ht */
                foreach ($n->getHashtags()->toArray() as $ht) {
                    $extHt = $this->getHashTagByName($ht->getName(), $hashTagsRepo);
                    if ($extHt instanceof HashTag) {
                        $n->removeHashtag($ht);
                        $n->addHashtag($extHt);
                    }
                }
                $this->em->persist($n);
                $this->em->flush();
                $addedCount++;
            }
        }
        return $addedCount;
    }

    public function searchNews($kw, $hashTagName, $page = 1, $limit = 10)
    {
        $hashTag = null;
        $hashTagsRepo = $this->em->getRepository('AppBundle:HashTag');
        if ($hashTagName) {
            $hashTag = $hashTagsRepo->findOneBy(['name' => $hashTagName]);
            if (!$hashTag) {
                throw new NotFoundHttpException($this->translator->trans('Тег #%hashtag% не найден', [
                    '%hashtag%' => $hashTagName
                ]));
            }
        }

        $newsRepo = $this->em->getRepository('AppBundle:NewsItem');
        $newsQuery = $newsRepo->searchNews($kw, $hashTag, $limit);

        $page = max(1, (int)$page);
        return $this->paginator->paginate($newsQuery, $page, 10);
    }

    public function getPopularTags($kw)
    {
        $hashTagsRepo = $this->em->getRepository('AppBundle:HashTag');
        return $hashTagsRepo->getPopularHashTags($kw);
    }

    protected function isSourceIdExists($sourceId, $newsRepo)
    {
        return $newsRepo->findOneBy(['sourceId' => $sourceId]) instanceof NewsItem;
    }

    protected function getHashTagByName($name, $hashTagsRepo)
    {
        return $hashTagsRepo->findOneBy(['name' => $name]);
    }

}
