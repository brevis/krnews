<?php

namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Command\LockableTrait;

class NewsCollectCommand extends ContainerAwareCommand
{
    use LockableTrait;

    const DEFAULT_SLEEP = 5;

    protected function configure()
    {
        $this
            ->setName('news:collect')
            ->setDescription('Collect news')
            ->addOption('daemon', null, InputOption::VALUE_NONE, 'Run in daemon mode')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        if (!$this->lock()) {
            $output->writeln('The command is already running in another process.');
            return 0;
        }

        $isDaemonMode = $input->getOption('daemon');

        $sleep = abs($this->getContainer()->getParameter('collect_news_daemon_sleep'));
        if (!is_int($sleep)) {
            $sleep = self::DEFAULT_SLEEP;
        }

        /** @var \AppBundle\Service\NewsService */
        $newsService = $this->getContainer()->get('news_service');

        while (true) {
            $output->writeln(sprintf("[%s] Start collect news.", (new \DateTime())->format('H:i:s')));

            try {
                $addedCount = $newsService->downloadNews();
                $output->writeln(sprintf("%d news added.", $addedCount));
            } catch (\Exception $e) {
                $output->writeln(sprintf("<error>%s</error>", $e->getMessage()));
            }

            if ($isDaemonMode) {
                $output->writeln("Wait {$sleep} seconds...");
                sleep($sleep);
            } else {
                break;
            }
        }

        $output->writeln('Done.');

        $this->release();
    }

}
