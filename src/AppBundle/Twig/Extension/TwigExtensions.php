<?php

namespace AppBundle\Twig\Extension;

use Symfony\Component\Translation\TranslatorInterface;
use Symfony\Component\Routing\Router;

class TwigExtensions extends \Twig_Extension
{

    /** @var TranslatorInterface */
    protected $translator;

    /** @var Router */
    protected $router;

    public function setTranslator(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    public function setRouter(Router $router)
    {
        $this->router = $router;
    }

    public function getName()
    {
        return 'news_twig_extension';
    }

    public function getFilters()
    {
        return array(
            new \Twig_SimpleFilter('human_date', [$this, 'humanDateFilter'], ['needs_environment' => false]),
            new \Twig_SimpleFilter('highlight_kw', [$this, 'highlightKwFilter'], ['needs_environment' => false]),
            new \Twig_SimpleFilter('clickable_hashtags', [$this, 'clickableHashtagsFilter'], ['needs_environment' => false]),
            new \Twig_SimpleFilter('clickable_urls', [$this, 'clickableUrlsFilter'], ['needs_environment' => false]),
        );
    }

    public function humanDateFilter(\DateTime $date, $withTime = false)
    {
        $today = new \DateTime();
        $yesterday = clone $today;
        $yesterday->modify('-1 day');
        $beforeYesterday = clone $today;
        $beforeYesterday->modify('-2 day');

        $format = '%s, %d %s%s';
        $time = $withTime ? ' ' . $date->format('H:i') : '';
        if ($today->format('Y-m-d') === $date->format('Y-m-d')) {
            $label = $this->translator->trans('Сегодня');
            $humanDate = sprintf($format, $label, $date->format('d'), $this->getMonthName($date->format('m')), $time);
        } elseif ($yesterday->format('Y-m-d') === $date->format('Y-m-d')) {
            $label = $this->translator->trans('Вчера');
            $humanDate = sprintf($format, $label, $date->format('d'), $this->getMonthName($date->format('m')), $time);
        } elseif ($beforeYesterday->format('Y-m-d') === $date->format('Y-m-d')) {
            $label = $this->translator->trans('Позавчера');
            $humanDate = sprintf($format, $label, $date->format('d'), $this->getMonthName($date->format('m')), $time);
        } else {
            $humanDate = $date->format('d.m.Y') . $time;
        }

        return $humanDate;
    }

    public function highlightKwFilter($content, $kw = '')
    {
        if ($kw === '') {
            return $content;
        }
        return preg_replace("/\w*?" . preg_quote($kw) . "\w*/ui", "<span class='highlight'>$0</span>", $content);
    }

    public function clickableHashtagsFilter($content)
    {
        $router = $this->router;
        return preg_replace_callback("/#([a-zа-яё0-9-_]+)/ui", function ($hashTag) use ($router) {
            return '<a href="' . $router->generate('hashtagpage', ['hashTag' => $hashTag[1]]) . '">#' . $hashTag[1] . '</a>';
        }, $content);
    }

    public function clickableUrlsFilter($content)
    {
        return preg_replace('!(((f|ht)tp(s)?://)[-a-zA-Zа-яА-Я()0-9@:%_+.~#?&;//=]+)!i', '<a href="$1" target="_blank">$1</a>', $content);

    }

    protected function getMonthName($monthIndex)
    {
        $monthNames = ["января", "февраля", "марта", "апреля",
                       "мая", "июня", "июля", "августа",
                       "сентября", "октября", "ноября", "декабря"];

        $monthIndex = (int) $monthIndex;
        return isset($monthNames[$monthIndex - 1]) ? $monthNames[$monthIndex - 1] : '';
    }
}