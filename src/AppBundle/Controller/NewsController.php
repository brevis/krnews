<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Service\NewsService;


class NewsController extends Controller
{
    const NEWS_PER_PAGE = 20;
    const HASH_TAGS_COUNT = 10;
    const HASH_TAGS_DAYS_LIMIT = 20;

    /** @var NewsService */
    protected $newsService;

    public function setNewsService(NewsService $newsService)
    {
        $this->newsService = $newsService;
    }

    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request, $hashTag = null)
    {
        $translator = $this->get('translator');
        $kw = $request->query->get('kw', '');
        $formKw = $kw;
        if ($kw !== '' && mb_strlen($kw, 'UTF-8') < 3) {
            $kw = '';
            $this->addFlash('warning', $translator->trans('Поисковая фраза должна состоять минимум из 3х символов.'));
        }
        $page = $request->query->getInt('page', 1);
        return $this->render($hashTag ?  'default/hashTag.html.twig' : 'default/index.html.twig', [
            'kw' => $kw,
            'formKw' => $formKw,
            'hashTag' => $hashTag,
            'newsPagination' => $this->newsService->searchNews($kw, $hashTag, $page),
            'hashTags' => $this->newsService->getPopularTags($kw)
        ]);
    }

    /**
     * @Route("/{hashTag}", requirements={"hashTag" = "[а-яА-ЯёЁa-zA-Z0-9\.\-\&]+"}, name="hashtagpage")
     */
    public function hashtagAction(Request $request, $hashTag)
    {
        return $this->indexAction($request, $hashTag);
    }

}
