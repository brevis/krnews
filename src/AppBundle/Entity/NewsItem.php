<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * NewsItem
 *
 * @ORM\Table(name="newsitem")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\NewsItemRepository")
 */
class NewsItem
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="content", type="string")
     */
    private $content;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="published_at", type="datetime")
     */
    private $publishedAt;

    /**
     * @var Media[]|ArrayCollection
     *
     * @ORM\OneToMany(
     *      targetEntity="Media",
     *      mappedBy="newsItem",
     *      orphanRemoval=true,
     *      cascade={"persist"}
     * )
     */
    private $media;

    /**
     * @var HashTag[]|ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="HashTag", cascade={"persist"}, inversedBy="newsItems")
     * @ORM\JoinTable(
     *  name="newsitem_hashtag",
     *  joinColumns={
     *      @ORM\JoinColumn(name="newsitem_id", referencedColumnName="id")
     *  },
     *  inverseJoinColumns={
     *      @ORM\JoinColumn(name="hashtag_id", referencedColumnName="id")
     *  }
     * )
     */
    private $hashtags;

    /**
     * @var int
     *
     * @ORM\Column(name="source_id", type="bigint")
     */
    private $sourceId;

    public function __construct()
    {
        $this->publishedAt = new \DateTime();
        $this->media = new ArrayCollection();
        $this->hashtags = new ArrayCollection();
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set content
     *
     * @param string $content
     *
     * @return NewsItem
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set publishedAt
     *
     * @param \DateTime $publishedAt
     *
     * @return NewsItem
     */
    public function setPublishedAt($publishedAt)
    {
        $this->publishedAt = $publishedAt;

        return $this;
    }

    /**
     * Get publishedAt
     *
     * @return \DateTime
     */
    public function getPublishedAt()
    {
        return $this->publishedAt;
    }

    /**
     * Set sourceId
     *
     * @param integer $sourceId
     *
     * @return NewsItem
     */
    public function setSourceId($sourceId)
    {
        $this->sourceId = $sourceId;

        return $this;
    }

    /**
     * Get sourceId
     *
     * @return integer
     */
    public function getSourceId()
    {
        return $this->sourceId;
    }

    /**
     * Add medium
     *
     * @param \AppBundle\Entity\Media $medium
     *
     * @return NewsItem
     */
    public function addMedia(\AppBundle\Entity\Media $medium)
    {
        $this->media[] = $medium;

        return $this;
    }

    /**
     * Remove medium
     *
     * @param \AppBundle\Entity\Media $medium
     */
    public function removeMedia(\AppBundle\Entity\Media $medium)
    {
        $this->media->removeElement($medium);
    }

    /**
     * Get media
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMedia()
    {
        return $this->media;
    }

    /**
     * Add hashtag
     *
     * @param \AppBundle\Entity\HashTag $hashtag
     *
     * @return NewsItem
     */
    public function addHashtag(\AppBundle\Entity\HashTag $hashtag)
    {
        $this->hashtags[] = $hashtag;

        return $this;
    }

    /**
     * Remove hashtag
     *
     * @param \AppBundle\Entity\HashTag $hashtag
     */
    public function removeHashtag(\AppBundle\Entity\HashTag $hashtag)
    {
        $this->hashtags->removeElement($hashtag);
    }

    /**
     * Get hashtags
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getHashtags()
    {
        return $this->hashtags;
    }
}
