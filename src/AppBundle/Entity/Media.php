<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Media
 *
 * @ORM\Table(name="media")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\MediaRepository")
 */
class Media
{

    const TYPE_IMAGE = 1;

    const TYPE_VIDEO = 2;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="type", type="integer")
     */
    private $type;

    /**
     * @var string
     *
     * @ORM\Column(name="uri", type="string", length=255)
     */
    private $uri;

    /**
     * @var NewsItem
     *
     * @ORM\ManyToOne(targetEntity="NewsItem", inversedBy="media")
     * @ORM\JoinColumn(nullable=false)
     */
    private $newsItem;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set type
     *
     * @param integer $type
     *
     * @return Media
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return integer
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set uri
     *
     * @param string $uri
     *
     * @return Media
     */
    public function setUri($uri)
    {
        $this->uri = $uri;

        return $this;
    }

    /**
     * Get uri
     *
     * @return string
     */
    public function getUri()
    {
        return $this->uri;
    }

    /**
     * Set newsItem
     *
     * @param \AppBundle\Entity\NewsItem $newsItem
     *
     * @return Media
     */
    public function setNewsItem(\AppBundle\Entity\NewsItem $newsItem)
    {
        $this->newsItem = $newsItem;

        return $this;
    }

    /**
     * Get newsItem
     *
     * @return \AppBundle\Entity\NewsItem
     */
    public function getNewsItem()
    {
        return $this->newsItem;
    }
}
