<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * HashTag
 *
 * @ORM\Table(name="hashtag")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\HashTagRepository")
 */
class HashTag
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, unique=true)
     */
    private $name;

    /**
     * @var NewsItem[]|ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="NewsItem", mappedBy="hashtags")
     */
    private $newsItems;

    public function __construct()
    {
        $this->newsItems = new ArrayCollection();
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return HashTag
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Add newsItem
     *
     * @param \AppBundle\Entity\NewsItem $newsItem
     *
     * @return HashTag
     */
    public function addNewsItem(\AppBundle\Entity\NewsItem $newsItem)
    {
        $this->newsItems[] = $newsItem;

        return $this;
    }

    /**
     * Remove newsItem
     *
     * @param \AppBundle\Entity\NewsItem $newsItem
     */
    public function removeNewsItem(\AppBundle\Entity\NewsItem $newsItem)
    {
        $this->newsItems->removeElement($newsItem);
    }

    /**
     * Get newsItems
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getNewsItems()
    {
        return $this->newsItems;
    }
}
